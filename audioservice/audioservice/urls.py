from django.urls import path

from audioservice import views

urlpatterns = [
    path('tracks/', views.TrackListView.as_view(), name="track_list"),
    path('tracks/upload/', views.TrackUploadView.as_view(), name="track_upload"),
    path('tracks/<int:track_id>/', views.TrackDownloadView.as_view(), name="track_download"),
    path('tracks/forbidden/', views.ForbiddenTracksUploadView.as_view(), name="track_forbidden"),
]
