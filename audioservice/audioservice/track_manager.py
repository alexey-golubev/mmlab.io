import datetime
import tempfile
import os

from django.db import transaction, models

import mutagen
import xlrd

from audioservice.models import Track, TrackData, Album, Artist


class TrackManager:

    @classmethod
    def from_settings(cls):
        return cls()

    def create_forbidden_track(self, artist, name):
        with transaction.atomic():
            artist, _ = Artist.objects.get_or_create(name=artist)
            track, created = Track.objects.get_or_create(artists__in=[artist], name=name)
            if created:
                track.artists.add(artist)
            track.forbidden = True
            track.save()

    def check_tracks(self, file):
        fd, tmp = tempfile.mkstemp()
        with os.fdopen(fd, 'wb') as tmp_file:
            tmp_file.write(file.read())
        workbook = xlrd.open_workbook(tmp)
        sh = workbook.sheet_by_index(0)
        for idx in range(1, sh.nrows):
            artist = sh.row(idx)[0].value
            track = sh.row(idx)[1].value
            self.create_forbidden_track(artist=artist, name=track)

    def create_album(self, name, release_date, artists):
        if not name:
            return None

        matching_albums = Album.objects.filter(name=name).annotate(
            artists_count=models.Count("artists")
        ).filter(artists_count=len(artists))
        for artist in artists:
            matching_albums = matching_albums.filter(artists=artist)
            if release_date:
                matching_albums = matching_albums.objects.filter(release_date=release_date)

        if matching_albums:
            album = matching_albums[0]
        else:
            with transaction.atomic():
                album = Album.objects.create(
                    name=name,
                    release_date=release_date,
                )
            album.artists.set(artists)
        return album

    def create_track(self, name, data_file, format,
                     artists=None, duration=None, album=None, **kwargs):

        with transaction.atomic():
            track_data = TrackData.objects.create(
                content=data_file
            )
            track, _ = Track.objects.get_or_create(
                name=name
            )

            duration = datetime.timedelta(seconds=duration)
            attrs = {
                'data': track_data,
                'duration': duration,
                'format': format,
                'album': album,
                **kwargs
            }

            for name, value in attrs.items():
                setattr(track, name, value)
            track.save()

            if artists:
                track.artists.set(artists)
        return track

    def parse_file(self, data_file):
        mfile = mutagen.File(data_file)
        res = {}
        for key, synonyms in [
                ("albums", ("album", "TALB")),
                ("titles", ("title", "TIT2")),
                ("artists", ("artist", "TPE2")),
        ]:
            for s in synonyms:
                if s in mfile:
                    res[key] = mfile[s]
        res["duration"] = mfile.info.length
        res["format"] = mfile.info.__class__.__module__.split(".")[-1]
        return res
