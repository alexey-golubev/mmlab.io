from rest_framework import serializers

from audioservice.models import Track, Album, Artist


class ArtistSerializer(serializers.ModelSerializer):

    class Meta:
        model = Artist
        fields = "__all__"


class AlbumSerializer(serializers.ModelSerializer):

    class Meta:
        model = Album
        exclude = ("artists",)


class TrackSerializer(serializers.ModelSerializer):

    class Meta:
        model = Track
        exclude = ("data",)

    album = AlbumSerializer()
    artists = ArtistSerializer(many=True)


class TracksUploadSerializer(serializers.Serializer):
    file = serializers.FileField()
    forbidden = serializers.BooleanField(default=False)
