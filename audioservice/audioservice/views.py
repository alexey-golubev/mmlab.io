from django.shortcuts import get_object_or_404
from rest_framework import generics, views, parsers, response
from django.http import HttpResponse

from audioservice import serializers
from audioservice.models import Track, Artist
from audioservice.track_manager import TrackManager


class TrackListView(generics.ListAPIView):
    queryset = Track.objects.all()
    serializer_class = serializers.TrackSerializer


class TrackDownloadView(views.APIView):

    def get(self, request, track_id):
        track = get_object_or_404(Track, id=track_id, forbidden=False)
        resp = HttpResponse(track.data.content, content_type="application/force-download")
        resp["Content-Disposition"] = f'attachment; filename="{track.name}.{track.format}"'
        resp["Content-type"] = "Content-Type: audio/mpeg"
        return resp


class TrackUploadView(views.APIView):
    serializer_class = serializers.TracksUploadSerializer

    def put(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data_file = request.data['file']

        track_manager = TrackManager.from_settings()
        parsed = track_manager.parse_file(data_file)

        artist_names = parsed.get("artists", [])
        album_names = parsed.get("albums")
        album_name = album_names[0] if album_names else None
        track_names = parsed.get("titles")
        track_name = track_names[0] if track_names else None
        track_duration = parsed["duration"]
        track_format = parsed["format"]

        artists = [
            Artist.objects.get_or_create(name=name)[0]
            for name in artist_names
        ]
        album = track_manager.create_album(
            album_name, None, artists)

        track = track_manager.create_track(
            track_name, data_file,
            track_format, artists=artists,
            duration=track_duration, album=album,
            forbidden=serializer.validated_data.get('forbidden')
        )

        return response.Response(serializers.TrackSerializer(track).data)


class ForbiddenTracksUploadView(views.APIView):
    parser_classes = [parsers.FileUploadParser]

    def post(self, request):
        file = request.data['file']
        track_manager = TrackManager.from_settings()
        track_manager.check_tracks(file)
        return response.Response(status=204)
